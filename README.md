# crx-inteview
This project uses small boilerplate with basic configuration
and common npm packages developed by Michael Motornyi

## How to run:
- npm install
- npm start
- in your browser navigate to http://localhost:8080/

## Project is not finished
- Some code needs to be refactored
- Fix TODOs
- Add routes and move search to a seperate page (use hash router)
- Add styling to select-filed component and fix styles in user-search component
