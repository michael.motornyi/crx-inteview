// This is mocked methods. Should be replaced with real api request

const sleep = time =>
  new Promise(resolve => setTimeout(resolve, time));

export const signInAdmin = async adminName => {
  const mockedUsers = ['crudadmin', 'readonlyadmin'];

  // Added sleep method for more realistic
  await sleep(1000);

  if (mockedUsers.includes(adminName)) {
    return { message: 'Successfully signed in' };
  }
  throw Error('Admin id is invalid');
};

export const ROLES = { WRITE: 'write', READ: 'read' };

export const getAdminRoles = async adminName => {
  const rolesData = [
    { user: 'crudadmin', roles: [ROLES.READ, ROLES.WRITE] },
    { user: 'readonlyadmin', roles: [ROLES.READ] },
  ];

  // Added sleep method for more realistic
  await sleep(1000);

  const result = rolesData.find(data => data.user === adminName);
  if (result) {
    return result?.roles;
  }

  throw Error('No roles found');
};
