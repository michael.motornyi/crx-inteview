// This is mocked methods. Should be replaced with real api request

export const getUsers = async () => {
  const URL =
    'http://www.mocky.io/v2/5d7f3d17330000204ef0b027?mocky-delay=500ms';

  const result = await fetch(URL);
  return result.json();
};

export const getCountryNameByCode = code => {
  const countries = {
    de: 'Germany',
    ru: 'Russia',
  };

  return countries[code] || '';
};
