import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/app/app.component';

import './index.css';

const title = 'CRX Inteview';

ReactDOM.render(
  <App title={title} />,
  document.getElementById('app'),
);

module.hot.accept();
