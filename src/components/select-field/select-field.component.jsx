import React, { useState } from 'react';

const SelectField = ({ options, onSelect, initialValue }) => {
  const [selected, setSelected] = useState(initialValue);

  const handleSelect = event => {
    setSelected(event.target.value);
    if (onSelect) {
      onSelect(selected);
    }
  };

  return (
    <select value={selected} onChange={handleSelect}>
      {options &&
        options.map(option => (
          <option value={option}>{option}</option>
        ))}
    </select>
  );
};

export default SelectField;
