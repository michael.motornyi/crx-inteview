import React, { useState, useRef } from 'react';
import cn from 'classnames';
import { signInAdmin } from '../../api/admin.api';

import s from './login-form.styles.json';

// TODO: Use intl service
const LOGIN_FORM_LABEL = 'CRX Test';
const ADMIN_ID_LABEL = 'Admin Id';
const SIGN_IN_BUTTON_TEXT = 'Sign in';

const LoginForm = ({ onLogin }) => {
  const [message, setMessage] = useState({ error: false, text: '' });
  const [loading, setLoading] = useState(false);

  const adminIdRef = useRef('');

  const handleSignIn = async () => {
    setLoading(true);
    const adminName = adminIdRef.current.value;

    try {
      const result = await signInAdmin(adminName);
      setMessage({ error: false, text: result.message });
      if (onLogin) {
        onLogin(adminName);
      }
    } catch (e) {
      setMessage({ error: true, text: e.message });
    }
    setLoading(false);
  };

  return (
    <div className={cn(s.container)}>
      <div className={cn(s['form-label'])}>{LOGIN_FORM_LABEL}</div>
      <label htmlFor="admin-id" className={cn(s['adminid-label'])}>
        {ADMIN_ID_LABEL}
      </label>
      <input
        disabled={loading}
        ref={adminIdRef}
        name="admin-id"
        className={cn(s['adminid-input'])}
      />
      <div className={cn(s['submit-button-wrapper'])}>
        <div
          className={cn(
            message.error ? 'text-red-500' : 'text-green-500',
            'text-sm',
          )}
        >
          {message.text}
        </div>
        <button
          type="button"
          disabled={loading}
          className={cn(
            s['submit-button'],
            loading ? '' : 'hover:bg-blue-600',
          )}
          onClick={handleSignIn}
        >
          {SIGN_IN_BUTTON_TEXT}
        </button>
      </div>
    </div>
  );
};

export default LoginForm;
