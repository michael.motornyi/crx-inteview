/* eslint-disable react/jsx-curly-newline */
import React, { useState, useEffect } from 'react';
import { useEffectOnce } from 'react-use';
import cn from 'classnames';

import { getUsers, getCountryNameByCode } from '../../api/users.api';
import { ROLES } from '../../api/admin.api';
import Filter from '../filter/filter.component';
import SelectField from '../select-field/select-field.component';
import s from './user-search.styles.json';

// TODO: Use configuration service
const COUNTRIES_OPTIONS = ['de', 'ru'];
const SEARCH_FIELD = 'lastname';
const SORT_FIELD = 'lastname';
const TABLE_COLUMNS = {
  firstname: 'First name',
  lastname: 'Last name',
  country: 'Country',
};

const ASC = 'asc';
const DESC = 'desc';

const sort = (initialData, order) => {
  if (initialData?.length) {
    const data = [...initialData];
    data.sort((a, b) => a[SORT_FIELD].localeCompare(b[SORT_FIELD]));
    if (order === DESC) {
      data.reverse();
    }
    return data;
  }
  return null;
};

const UserSearch = ({ roles, initialSortOrder, onSetSortOreder }) => {
  const [usersData, setUsersData] = useState(null);
  const [filteredUsers, setFilteredUsers] = useState(null);
  const [sortOrder, setSortOrder] = useState(initialSortOrder);

  // Update sort order
  useEffect(() => {
    setSortOrder(initialSortOrder);
  }, [initialSortOrder]);

  // Load users data
  useEffectOnce(() => {
    (async () => {
      try {
        const result = await getUsers();
        // I am using optional chaining here. It is the same as result && result.users
        setUsersData(result?.users);
      } catch (e) {
        // TODO: handle error
      }
    })();
  });

  const handleSort = (field, order) => {
    if (filteredUsers && field === SORT_FIELD) {
      setSortOrder(order);
      onSetSortOreder(order);
      setFilteredUsers(sort(filteredUsers, order));
    }
  };

  const handleSearch = users => {
    setFilteredUsers(sort(users, sortOrder));
  };

  const toggledSortValue = () => {
    return sortOrder === ASC ? DESC : ASC;
  };

  // This method chooses what component should be rendered
  // TODO: Refactor. Use configuration service
  const transformData = user => {
    return {
      ...user,
      country: roles.includes(ROLES.WRITE) ? (
        <SelectField
          options={COUNTRIES_OPTIONS.map(option =>
            getCountryNameByCode(option),
          )}
          initialValue={getCountryNameByCode(user.country)}
        />
      ) : (
        getCountryNameByCode(user.country)
      ),
    };
  };

  return (
    <>
      {roles && roles.includes(ROLES.READ) && (
        <div>
          <Filter
            data={usersData}
            field={SEARCH_FIELD}
            onSearch={handleSearch}
          />
          {filteredUsers && (
            <table className={cn(s.table)}>
              <thead>
                <tr>
                  {Object.keys(TABLE_COLUMNS).map(columnName => (
                    <th
                      className={cn(s['table-header'])}
                      onClick={() =>
                        handleSort(columnName, toggledSortValue())
                      }
                    >
                      {TABLE_COLUMNS[columnName]}
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {filteredUsers &&
                  filteredUsers.map(user => (
                    <tr>
                      {Object.keys(TABLE_COLUMNS).map(columnName => (
                        <td className={cn(s['table-row'])}>
                          {transformData(user)[columnName]}
                        </td>
                      ))}
                    </tr>
                  ))}
              </tbody>
            </table>
          )}
        </div>
      )}
    </>
  );
};

export default UserSearch;
