import React from 'react';
import cn from 'classnames';

import s from './filter.styles.json';

// TODO: Use intl service
const SEARCH_LABEL = 'Search by last name';
// TODO: Use configuration service
const SEARCH_FROM_LENGTH = 2;

const Filter = ({ data, field, onSearch }) => {
  const filterData = query => {
    if (data && query.length >= SEARCH_FROM_LENGTH) {
      return data.filter(item =>
        item[field].toLowerCase().includes(query),
      );
    }
    return [];
  };

  const handleChange = event => {
    const query = event.target.value;
    onSearch(filterData(query));
  };

  return (
    <div className={cn(s.container)}>
      <label className={cn(s['filter-label'])} htmlFor="search">
        {SEARCH_LABEL}
      </label>
      <input
        name="search"
        onChange={handleChange}
        className={cn(s['filter-input'])}
      />
    </div>
  );
};

export default Filter;
