import React, { useState, useEffect } from 'react';
import cn from 'classnames';
import s from './app.styles.json';

import LoginForm from '../login-form/login-form.component';
import UserSearch from '../user-search/user-search.component';
import { getAdminRoles } from '../../api/admin.api';

const App = () => {
  const [isLogged, setIsLogged] = useState(false);
  const [adminRoles, setAdminRoles] = useState([]);
  const [currentAdmin, setCurrentAdmin] = useState(null);
  const [sortOrder, setSortOrder] = useState(true);

  const loginHandler = async adminName => {
    setIsLogged(true);
    setCurrentAdmin(adminName);
    setAdminRoles(await getAdminRoles(adminName));
  };

  useEffect(() => {
    setSortOrder(localStorage.getItem(currentAdmin));
  }, [currentAdmin]);

  const handleSetSortOreder = order => {
    localStorage.setItem(currentAdmin, order);
  };

  return (
    <div className={cn(s.wrapper)}>
      <div className={cn(s['login-container'])}>
        <LoginForm onLogin={loginHandler} />
      </div>
      <div className={cn(s['search-container'])}>
        {isLogged && (
          <UserSearch
            roles={adminRoles}
            initialSortOrder={sortOrder}
            onSetSortOreder={handleSetSortOreder}
          />
        )}
      </div>
    </div>
  );
};

export default App;
